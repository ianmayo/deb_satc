package com.planetmayo.debrief.satc.model.generator;

import com.planetmayo.debrief.satc.model.contributions.BaseContribution;

public class ContributionChangedAdapter implements IContributionsChangedListener
{

	@Override
	public void added(BaseContribution contribution)
	{
	}

	@Override
	public void removed(BaseContribution contribution)
	{
	}
}
